# ics-ans-role-spinnaker

Ansible role to install [Spinnaker SDK](https://www.flir.com/products/spinnaker-sdk/).

## Role Variables

```yaml
spinnaker_group: flirimaging
spinnaker_users: []
spinnaker_rmem: "10485760"
```

The users from `spinnaker_users` will be added to the `spinnaker_group` to have access to USB hardware.

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-spinnaker
```

## License

BSD 2-clause
